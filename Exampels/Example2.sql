CREATE TABLE IF NOT EXISTS iot_temperature_sensor_data (
 ts timestamp without time zone,
 device_id text,
 reading float
 ) PARTITION BY LIST (device_id);



CREATE TABLE device_a PARTITION OF iot_temperature_sensor_data FOR VALUES IN ( 'a' ) PARTITION BY RANGE (ts);
CREATE TABLE device_b PARTITION OF iot_temperature_sensor_data FOR VALUES IN ( 'b' ) PARTITION BY RANGE (ts);
CREATE TABLE device_c PARTITION OF iot_temperature_sensor_data FOR VALUES IN ( 'c' ) PARTITION BY RANGE (ts);
CREATE TABLE device_d PARTITION OF iot_temperature_sensor_data FOR VALUES IN ( 'd' ) PARTITION BY RANGE (ts);
CREATE TABLE device_e PARTITION OF iot_temperature_sensor_data FOR VALUES IN ( 'e' ) PARTITION BY RANGE (ts);

CREATE TABLE iot_temperature_sensor_data_device_a_2021_february PARTITION OF device_a FOR VALUES FROM ('2021-02-01') TO ('2021-03-01');
CREATE TABLE iot_temperature_sensor_data_device_a_2021_march PARTITION OF device_a FOR VALUES FROM ('2021-03-01') TO ('2021-04-01');

CREATE TABLE iot_temperature_sensor_data_device_b_2021_february PARTITION OF device_b FOR VALUES FROM ('2021-02-01') TO ('2021-03-01');
CREATE TABLE iot_temperature_sensor_data_device_b_2021_march PARTITION OF device_b FOR VALUES FROM ('2021-03-01') TO ('2021-04-01');

CREATE SCHEMA partman;
CREATE EXTENSION pg_partman WITH SCHEMA partman;

SELECT partman.create_parent('public.iot_temperature_sensor_data','ts','native','monthly');
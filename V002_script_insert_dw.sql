-- INSERT category_types
INSERT INTO events_datawarehouse.category_types(id, name) VALUES (1, 'Update Event');
INSERT INTO events_datawarehouse.category_types(id, name) VALUES (2, 'Command Message');
INSERT INTO events_datawarehouse.category_types(id, name) VALUES (3, 'ValidBio Legacy');

---------------------------------------------------------------------------------------

-- INSERT aggregation_types

INSERT INTO events_datawarehouse.aggregation_types(id, name) VALUES (0, 'Unbounded');
INSERT INTO events_datawarehouse.aggregation_types(id, name) VALUES (1, 'Subject');
INSERT INTO events_datawarehouse.aggregation_types(id, name) VALUES (2, 'Batch file');

---------------------------------------------------------------------------------------

-- INSERT metric_types

INSERT INTO events_datawarehouse.metric_types(id, name)	VALUES (1, 'Biometric face');
INSERT INTO events_datawarehouse.metric_types(id, name)	VALUES (2, 'Biometric finger');
INSERT INTO events_datawarehouse.metric_types(id, name)	VALUES (3, 'Biographic data');

---------------------------------------------------------------------------------------

-- INSERT categories
INSERT INTO events_datawarehouse.categories(id, category_type_id, name, create_timestamp, census_timestamp, census_count) VALUES (1, 3, 'Contract created', current_timestamp, current_timestamp, 0);
INSERT INTO events_datawarehouse.categories(id, category_type_id, name, create_timestamp, census_timestamp, census_count) VALUES (2, 3, 'Rules processed', current_timestamp, current_timestamp, 0);
INSERT INTO events_datawarehouse.categories(id, category_type_id, name, create_timestamp, census_timestamp, census_count) VALUES (3, 3, 'Agreements processed', current_timestamp, current_timestamp, 0);
INSERT INTO events_datawarehouse.categories(id, category_type_id, name, create_timestamp, census_timestamp, census_count) VALUES (4, 3, 'Score generated', current_timestamp, current_timestamp, 0);
  			
---------------------------------------------------------------------------------------

-- INSERT sources
INSERT INTO events_datawarehouse.sources(uuid, name, create_timestamp, census_timestamp, census_count) VALUES ('4b380be5-3ad5-4b91-8e49-df31ced87cd1', 'ValidBio', current_timestamp, current_timestamp, 0);

---------------------------------------------------------------------------------------

--Insert all dates for 10 year since 2018
INSERT INTO events_datawarehouse.dates
select 
		TO_CHAR(datum, 'yyyymmdd')::INT AS id,
		EXTRACT(YEAR FROM datum)::INT2 AS year,
		EXTRACT(MONTH FROM datum)::INT2 AS month,
		EXTRACT(DAY FROM datum)::INT2 AS day,
		EXTRACT(DOY FROM datum)::INT2 AS day_of_year,
		EXTRACT(ISODOW FROM datum)::INT2 AS day_of_week,
		EXTRACT(WEEK FROM datum)::INT2 AS week,
		EXTRACT(QUARTER FROM datum)::INT2 AS quarter,
		--for brazilian holidays
		CASE 
			WHEN to_char(datum, 'MMDD') IN ('0101', '0421', '0501', '0907', '1012', '1102', '1115', '1225')
			THEN TRUE 
			ELSE FALSE 
		END AS holiday,
		CASE
           WHEN EXTRACT(ISODOW FROM datum) IN (6, 7) 
		   THEN TRUE
           ELSE FALSE
        END AS weekend,
		current_timestamp as create_timestamp,   
		current_timestamp as census_timestamp,
		0::INT8 as census_count
FROM (
		-- 365 * 10 + 1 (for leap years)
		SELECT '2018-01-01'::DATE + SEQUENCE.DAY AS datum
    	FROM GENERATE_SERIES(0, 3651) AS SEQUENCE (DAY)
    	GROUP BY SEQUENCE.DAY ) DQ
ORDER BY 1;

---------------------------------------------------------------------------------------

--Insert all times
INSERT INTO events_datawarehouse.times
SELECT 
		extract(epoch from second)::INT as id,
		extract(second from second)::INT2 as second,
		extract(minute from second)::INT2 as minute,
		extract(hour from second)::INT2 as hour,
		current_timestamp as create_timestamp,   
		current_timestamp as census_timestamp,
		0::INT8 as census_count
FROM (
SELECT '0:00:00'::time + (sequence.second || ' seconds')::interval AS second
	FROM generate_series(0,86399) AS sequence(second)
	GROUP BY sequence.second) DQ
ORDER BY 1;

---------------------------------------------------------------------------------------
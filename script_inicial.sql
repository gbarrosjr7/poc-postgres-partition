CREATE SCHEMA IF NOT EXISTS partman;
CREATE SCHEMA IF NOT EXISTS events_datawarehouse;
CREATE EXTENSION IF NOT EXISTS pg_partman WITH SCHEMA partman;


CREATE SEQUENCE IF NOT EXISTS events_datawarehouse.dates_seq;

-- domain tables
CREATE TABLE IF NOT EXISTS events_datawarehouse.aggregation_types(
                                                                     id SERIAL,
                                                                     name VARCHAR(256) NOT NULL,
                                                                     CONSTRAINT aggregation_types_pk PRIMARY KEY(id)
);

CREATE TABLE IF NOT EXISTS events_datawarehouse.category_types(
                                                                  id SERIAL,
                                                                  name VARCHAR(256),
                                                                  CONSTRAINT category_types_pk PRIMARY KEY(id)
);
CREATE TABLE IF NOT EXISTS events_datawarehouse.metric_types(
                                                                id SERIAL,
                                                                name VARCHAR(256) NOT NULL,
                                                                CONSTRAINT metric_types_pk PRIMARY KEY(id)
);
CREATE TABLE IF NOT EXISTS events_datawarehouse.subject_types(
                                                                 id SERIAL,
                                                                 name VARCHAR(256) NOT NULL,
                                                                 CONSTRAINT subject_types_pk PRIMARY KEY(id)
);

-- dimension tables

CREATE TABLE IF NOT EXISTS events_datawarehouse.aggregations(
                                                                uuid UUID NOT NULL,
                                                                aggregation_type_id INTEGER NOT NULL,
                                                                name VARCHAR(256) NOT NULL,
                                                                create_timestamp TIMESTAMP NOT NULL,
                                                                census_timestamp TIMESTAMP NOT NULL,
                                                                census_count BIGINT NOT NULL,
                                                                reference BYTEA NOT NULL,
                                                                CONSTRAINT aggregations_pk PRIMARY KEY(uuid),
                                                                CONSTRAINT aggregation_types_fk FOREIGN KEY(aggregation_type_id) REFERENCES events_datawarehouse.aggregation_types(id)
) -- spectating at least 4 updates for each how
    WITH (FILLFACTOR = 25);

CREATE TABLE IF NOT EXISTS events_datawarehouse.categories(
                                                              id SERIAL,
                                                              category_type_id INTEGER NOT NULL,
                                                              name VARCHAR(256) NOT NULL,
                                                              create_timestamp TIMESTAMP NOT NULL,
                                                              census_timestamp TIMESTAMP NOT NULL,
                                                              census_count BIGINT NOT NULL,
                                                              CONSTRAINT categories_pk PRIMARY KEY(id),
                                                              CONSTRAINT category_types_fk FOREIGN KEY(category_type_id) REFERENCES events_datawarehouse.category_types(id)
) -- spectating at least 4 updates for each how
    WITH (FILLFACTOR = 25);

CREATE TABLE IF NOT EXISTS  events_datawarehouse.contracts(
                                                              uuid UUID NOT NULL,
                                                              name VARCHAR(256) NOT NULL,
                                                              create_timestamp TIMESTAMP NOT NULL,
                                                              census_timestamp TIMESTAMP NOT NULL,
                                                              census_count BIGINT NOT NULL,
                                                              CONSTRAINT contracts_pk PRIMARY KEY(uuid)
) -- spectating at least 4 updates for each how
    WITH (FILLFACTOR = 25);

CREATE TABLE IF NOT EXISTS  events_datawarehouse.dates(
                                                          id SERIAL,
                                                          year SMALLINT NOT NULL,
                                                          month SMALLINT NOT NULL,
                                                          day SMALLINT NOT NULL,
                                                          day_of_year SMALLINT NOT NULL,
                                                          day_of_week SMALLINT NOT NULL,
                                                          week SMALLINT NOT NULL,
                                                          quarter SMALLINT NOT NULL,
                                                          holiday BOOLEAN NOT NULL,
                                                          weekend BOOLEAN NOT NULL,
                                                          create_timestamp TIMESTAMP NOT NULL,
                                                          census_timestamp TIMESTAMP NOT NULL,
                                                          census_count BIGINT NOT NULL,
                                                          CONSTRAINT dates_pk PRIMARY KEY(id)
) -- spectating at least 4 updates for each how
    WITH (FILLFACTOR = 25);


CREATE TABLE IF NOT EXISTS  events_datawarehouse.mesures(
                                                            uuid UUID NOT NULL,
                                                            name VARCHAR(256) NOT NULL,
                                                            metric_type_id INTEGER NOT NULL,
                                                            start_range FLOAT NOT NULL,
                                                            end_range FLOAT not NULL,
                                                            create_timestamp TIMESTAMP NOT NULL,
                                                            change_timestamp TIMESTAMP NOT NULL,
                                                            census_timestamp TIMESTAMP NOT NULL,
                                                            census_count BIGINT NOT NULL,
                                                            CONSTRAINT mesures_pk PRIMARY KEY(uuid),
                                                            CONSTRAINT metric_types_fk FOREIGN KEY(metric_type_id) REFERENCES events_datawarehouse.metric_types(id)
) -- spectating at least 4 updates for each how
    WITH (FILLFACTOR = 25);

CREATE TABLE IF NOT EXISTS  events_datawarehouse.sources(
                                                            uuid UUID NOT NULL,
                                                            name VARCHAR(256) NOT NULL,
                                                            create_timestamp TIMESTAMP NOT NULL,
                                                            census_timestamp TIMESTAMP NOT NULL,
                                                            census_count BIGINT NOT NULL,
                                                            CONSTRAINT sources_pk PRIMARY KEY(uuid)
) -- spectating at least 4 updates for each how
    WITH (FILLFACTOR = 25);

CREATE TABLE IF NOT EXISTS  events_datawarehouse.subjects(
                                                             uuid UUID NOT NULL,
                                                             subject_type_id INTEGER NOT NULL,
                                                             create_timestamp TIMESTAMP NOT NULL,
                                                             change_timestamp TIMESTAMP NOT NULL,
                                                             census_timestamp TIMESTAMP NOT NULL,
                                                             census_count BIGINT NOT NULL,
                                                             reference BYTEA NOT NULL,
                                                             CONSTRAINT subjects_pk PRIMARY KEY(uuid),
                                                             CONSTRAINT subject_types_fk FOREIGN KEY(subject_type_id) REFERENCES events_datawarehouse.subject_types(id)
) -- we need at least 512 partitions, couse subjects will be a bit table too
    PARTITION BY HASH(uuid);

--TODO: Verificar esse trecho do script original
--WITH (FILLFACTOR = 25);

CREATE TABLE IF NOT EXISTS  events_datawarehouse.times(
                                                          id SERIAL,
                                                          second INTEGER NOT NULL,
                                                          minute SMALLINT NOT NULL,
                                                          hour SMALLINT NOT NULL,
                                                          create_timestamp TIMESTAMP NOT NULL,
                                                          census_timestamp TIMESTAMP NOT NULL,
                                                          census_count BIGINT NOT NULL,
                                                          CONSTRAINT times_pk PRIMARY KEY(id)
);

-- event facts tables
-- this table will not have any constraints to be a very fast ingestion
CREATE TABLE IF NOT EXISTS  events_datawarehouse.events(
                                                           uuid UUID NOT NULL, date_id INTEGER NOT NULL,
                                                           time_id INTEGER NOT NULL, category_id INTEGER NOT NULL,
                                                           source_uuid UUID NOT NULL, subject_uuid UUID NOT NULL,
                                                           aggregation_uuid UUID NOT null, event_timestamp TIMESTAMP NOT NULL,
                                                           create_timestamp TIMESTAMP NOT NULL,
                                                           duration INTEGER NOt NULL
) -- this is a trick. will have same result as range by event_timestamp
    PARTITION BY RANGE(date_id);

CREATE TABLE IF NOT EXISTS  events_datawarehouse.event_mesures(
                                                                  event_uuid UUID NOT NULL, mesure_uuid UUID NOT NULL,
                                                                  value FLOAT NOT NULL
) PARTITION BY HASH(event_uuid);

CREATE TABLE IF NOT EXISTS  events_datawarehouse.event_details(
                                                                  event_uuid UUID NOT NULL, details BYTEA NOT NULL
) PARTITION BY HASH(event_uuid);
